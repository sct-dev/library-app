package com.sct_dev.lib_sctce;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class Home extends Fragment {
    public static Home newInstance() {
        Home fragment = new Home();
        return fragment;
    }
    public static final String PREFS_NAME = "LoginPrefs";
    TextView welcome,header_wel,header_email,current,fine,tot,due;
    FloatingActionButton lgoutbtn;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View infview= inflater.inflate(R.layout.fragment_home, container, false);
        welcome=infview.findViewById(R.id.welcome);
        Bundle bundle=getActivity().getIntent().getExtras();
        current=infview.findViewById(R.id.current);
        tot=infview.findViewById(R.id.tot);
        fine=infview.findViewById(R.id.fine);
        due=infview.findViewById(R.id.due);
        welcome.setText("Welcome, "+bundle.getString("name"));
        SharedPreferences settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
        current.setText(settings.getString("current",""));
        tot.setText(settings.getString("tot",""));
        fine.setText("Rs."+settings.getString("fine",""));
        due.setText(settings.getString("due",""));


        lgoutbtn=(FloatingActionButton)infview.findViewById(R.id.floatlogout);
       // profilebtn=(FloatingActionButton)infview.findViewById(R.id.floatprofile);
        lgoutbtn.setOnClickListener((new View.OnClickListener(){
            @Override
            public void onClick(View view){
                SharedPreferences settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.clear();
                editor.commit();
                Intent out = new Intent(getActivity(),login.class);
                startActivity(out);
                getActivity().finish();
            }
        }));

        return infview;
    }
}