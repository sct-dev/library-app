package com.sct_dev.lib_sctce;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;

public  class search extends AppCompatActivity implements AdapterView.OnItemSelectedListener,View.OnClickListener{

    private Spinner spinner;
    private static final String[] paths = {"Book name", "Author"};
    public String crit;
    public EditText searchcrit;
    private Button button;
    public String booklist[];
    public String authorlist[];
    public String booknauthor[];
    public ListView listView;
    int i;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        searchcrit =(EditText) findViewById(R.id.searchCrit);

        spinner = (Spinner) findViewById(R.id.spinner2);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(search.this,
                android.R.layout.simple_spinner_item, paths);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        button=(Button) findViewById(R.id.butbooksearch);
        listView=(ListView)findViewById(R.id.list1);
        button.setOnClickListener(this);

    }

    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {

        switch (position) {
            case 0:
                crit="bname";
                searchcrit.setHint("Book Name");
                break;
            case 1:
                crit="author";
                searchcrit.setHint("Author");
                break;
            default:crit="bname";


        }
    }
    public void onNothingSelected(AdapterView<?> parent){


    }
    @Override
    public void onClick(View v){

                searchbook(crit,searchcrit.getText().toString());
    }
    public void searchbook(String crite,String searchText){
        if(searchText.equals(""))
            Toast.makeText(getApplicationContext(),"Enter Word for searching",Toast.LENGTH_SHORT).show();
        else{

            StringRequest stringRequest=new StringRequest(Request.Method.POST, "http://sctceonline.in/cs-dept-library/searchtest.php", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray result = jsonObject.getJSONArray("result");

                        booklist=new String[result.length()];
                        authorlist=new String[result.length()];
                        booknauthor=new String[result.length()];
                        for(i=0;i<result.length();i++){
                            JSONObject Data = result.getJSONObject(i);

                            booklist[i]=Data.getString("bname");
                            authorlist[i]=Data.getString("author");
                            booknauthor[i]=booklist[i]+" by "+authorlist[i];
                        }

                        Toast.makeText(getApplicationContext(),"got result"+result.length(),Toast.LENGTH_SHORT).show();
                        ArrayAdapter<String> adapter=new ArrayAdapter<String>(search.this,R.layout.listlay,booknauthor);

                        ListView listView =(ListView)findViewById(R.id.list1) ;
                        listView.setAdapter(adapter);
                        listView.setTextFilterEnabled(true);

                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            public void onItemClick(AdapterView<?> parent, View view,
                                                    int position, long id) {
                                Intent bukin=new Intent(search.this,bookinfo.class);
                                bukin.putExtra("bookname",booklist[position]);
                                bukin.putExtra("author",authorlist[position]);
                                // When clicked, show a toast with the TextView text
                                /*Toast.makeText(getApplicationContext(),
                                        ((TextView) view).getText(), Toast.LENGTH_SHORT).show();*/
                                startActivity(bukin);
                            }
                        });

                    } catch (JSONException e) {

                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(),"Cant search",Toast.LENGTH_SHORT).show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(),"Not able to Connect.. ,Please check your internet connection.",Toast.LENGTH_LONG).show();
                    error.printStackTrace();

                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> params=new HashMap<String, String>();
                    params.put("crit",crit);
                    params.put("value",searchcrit.getText().toString());
                    return params;
                }
            };
            MySingleton.getInstance(search.this).addToRequestQue(stringRequest);
        }
    }

}