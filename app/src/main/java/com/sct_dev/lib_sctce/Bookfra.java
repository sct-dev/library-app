package com.sct_dev.lib_sctce;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Bookfra extends Fragment {
    public static Bookfra newInstance() {
        Bookfra fragment = new Bookfra();
        return fragment;
    }
    ListView mybooklisview;
    public  String bookid[];
    public  String bookname[];
    public  String adno;
    public  String chdate[];
    public  String retdate[];
    int i;
    public Booktra mylist[];
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View infview= inflater.inflate(R.layout.fragment_bookfra, container, false);
        Bundle bundle=getActivity().getIntent().getExtras();
        adno=bundle.getString("adno");
        mybookstrans(adno);
        return infview;


    }

    public void mybookstrans(String ano){

        StringRequest stringRequest=new StringRequest(Request.Method.POST, "http://sctceonline.in/cs-dept-library/mybooktrans.php", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                ArrayList<Booktra> mybooktr = new ArrayList<Booktra>();
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("result");

                    bookid=new String[result.length()];
                    bookname=new String[result.length()];
                    chdate=new String[result.length()];
                    retdate=new String[result.length()];
                    for(i=0;i<result.length();i++){
                        JSONObject Data = result.getJSONObject(i);
                        bookname[i]=Data.getString("bname");
                        bookid[i]=Data.getString("bid")+"  "+bookname[i];
                        chdate[i]="Checkout: "+Data.getString("chdate");
                        retdate[i]="Return: "+Data.getString("retdate");
                    }



                    mylist=new Booktra[result.length()];
                    for(i=0;i<result.length();i++){
                       mylist[i]= new Booktra(bookid[i],chdate[i],retdate[i]);
                       // mylist[i]= new Booktra("a","b","c");
                        mybooktr.add(mylist[i]);
                    }
// Create the adapter to convert the array to views
                    Toast.makeText(getActivity(),"got result"+result.length(),Toast.LENGTH_SHORT).show();
                    UsersAdapter adapter = new UsersAdapter(getActivity(), mybooktr);

// Attach the adapter to a ListView

                    ListView listView = (ListView) getView().findViewById(R.id.mybookslist);
                    final BottomNavigationView mynv=getActivity().findViewById(R.id.navigation);
                    listView.setOnScrollListener(new AbsListView.OnScrollListener() {
                        @Override
                        public void onScrollStateChanged(AbsListView absListView, int i) {
                            if(i==0){
                                mynv.setVisibility(View.VISIBLE);
                            }
                            else{
                                mynv.setVisibility(View.GONE);
                            }
                        }

                        @Override
                        public void onScroll(AbsListView absListView, int i, int i1, int i2) {

                        }


                    });

                    listView.setAdapter(adapter);


                } catch (JSONException e) {

                   // e.printStackTrace();
                    Toast.makeText(getActivity(),"Cant fetch",Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),"Not able to Connect.. ,Please check your internet connection.",Toast.LENGTH_LONG).show();
                error.printStackTrace();

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<String, String>();
                params.put("adno",adno);
                return params;
            }
        };
        MySingleton.getInstance(getActivity()).addToRequestQue(stringRequest);

    }
}