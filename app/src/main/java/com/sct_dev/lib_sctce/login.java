package com.sct_dev.lib_sctce;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class login extends AppCompatActivity {
    public static final String PREFS_NAME = "LoginPrefs";
    Button b;
    TextView textView;
    EditText User_name,Password;
    String user_name,password;
    AlertDialog.Builder builder;
    String login_url="http://sctceonline.in/cs-dept-library/LIB-SCTCE/login.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        if (settings.getString("logged", "").toString().equals("logged")) {
            Intent intent = new Intent(login.this, Homenav.class);
            Bundle bundle=new Bundle();
            bundle.putString("name",settings.getString("name","").toString());
            bundle.putString("email",settings.getString("email","").toString());
            bundle.putString("adno",settings.getString("adno","").toString());
            intent.putExtras(bundle);
            startActivity(intent);
            finish();
        }
        b=(Button)findViewById(R.id.button1);
        textView=(TextView)findViewById(R.id.tv_signup);
        User_name=(EditText)findViewById(R.id.log_Username);
        Password=(EditText)findViewById(R.id.log_password);
        builder=new AlertDialog.Builder(login.this);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                user_name=User_name.getText().toString();
                password=Password.getText().toString();

                if(user_name.equals("")||password.equals("")){
                    builder.setTitle("Something went wrong");
                    displayAlert("Enter a valid username or password.");
                }
                else{
                    StringRequest stringRequest=new StringRequest(Request.Method.POST, login_url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONArray jsonArray=new JSONArray(response);
                                JSONObject jsonObject=jsonArray.getJSONObject(0);
                                String code=jsonObject.getString("code");
                                if(code.equals("login_failed")){
                                    builder.setTitle("Login Error");
                                    displayAlert(jsonObject.getString("message"));
                                }
                                else{
                                    SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
                                    SharedPreferences.Editor editor = settings.edit();
                                    editor.putString("logged", "logged");
                                    editor.putString("adno",jsonObject.getString("adno"));
                                    editor.putString("name",jsonObject.getString("name"));
                                    editor.putString("email",jsonObject.getString("email"));
                                    editor.putString("fine",jsonObject.getString("fine"));
                                    editor.putString("current",jsonObject.getString("current"));
                                    editor.putString("tot",jsonObject.getString("tot"));
                                    editor.putString("due",jsonObject.getString("due"));
                                    editor.commit();
                                    if(jsonObject.getString("status").equals("yes"))
                                    {
                                        Intent intent=new Intent(login.this,Homenav.class);
                                        Bundle bundle=new Bundle();
                                        bundle.putString("name",jsonObject.getString("name"));
                                        bundle.putString("email",jsonObject.getString("email"));
                                        bundle.putString("adno",jsonObject.getString("adno"));
                                        intent.putExtras(bundle);
                                        startActivity(intent);
                                        finish();
                                    }
                                    else
                                        {
                                            editor.clear();
                                            editor.commit();
                                            Toast.makeText(getApplicationContext(),"User Registration Not Verified,Try After Sometime",Toast.LENGTH_SHORT).show();
                                        }
                                }
                            } catch (JSONException e) {

                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getApplicationContext(),"Not able to Connect.. ,Please check your internet connection.",Toast.LENGTH_LONG).show();
                            error.printStackTrace();

                        }
                    }){
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String,String> params=new HashMap<String, String>();
                            params.put("user_name",user_name);
                            params.put("password",password);
                            return params;
                        }
                    };
                    MySingleton.getInstance(login.this).addToRequestQue(stringRequest);
                }

            }

        });

    }

    private void displayAlert(String message){
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                User_name.setText("");
                Password.setText("");
            }
        });
        AlertDialog alertDialog= builder.create();
        alertDialog.show();
    }










    public void doThis(View v){
        if(R.id.tv_signup==v.getId()){
            Intent i2 = new Intent(this, signup.class);
            startActivity(i2);
        }
    }
}
