package com.sct_dev.lib_sctce;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class Profilefra extends Fragment {
    public static final String PREFS_NAME = "LoginPrefs";
    TextView usrname,mailid,admsno;
    public static Profilefra newInstance() {
        Profilefra fragment = new Profilefra();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View infview= inflater.inflate(R.layout.fragment_profilefra, container, false);
        usrname=infview.findViewById(R.id.usrname);
        mailid=infview.findViewById(R.id.mailid);
        admsno=infview.findViewById(R.id.admsno);
        SharedPreferences settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
        usrname.setText(settings.getString("name",""));
        mailid.setText(settings.getString("email",""));
        admsno.setText(settings.getString("adno",""));
        return infview;
    }
}