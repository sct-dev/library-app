package com.sct_dev.lib_sctce;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;



public class mainhome extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    public static final String PREFS_NAME = "LoginPrefs";
    TextView welcome,header_wel,header_email;
   // MagicButton mg1,mg2,mg3;
    FloatingActionButton fb1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainhome);
        mDrawerLayout=(DrawerLayout)findViewById(R.id.drawer);
        mToggle= new ActionBarDrawerToggle(this,mDrawerLayout,R.string.open,R.string.close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        /*
        Button button1;
        button1=(Button)findViewById(R.id.button6);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mainhome.this , checkouts.class);
                startActivity(intent);
            }
        });

        Button button2;
        button2=(Button)findViewById(R.id.button5);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mainhome.this , search.class);
                startActivity(intent);
            }
        });

        Button button3;
        button3=(Button)findViewById(R.id.button4);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mainhome.this , mybooks.class);
                startActivity(intent);
            }
        });

        */
        /*mg1=(MagicButton)findViewById(R.id.magic_button);
        mg1.setMagicButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mainhome.this , checkouts.class);
                startActivity(intent);
            }
        });

        mg2=(MagicButton)findViewById(R.id.magic_button2);
        mg2.setMagicButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mainhome.this ,search.class);
                startActivity(intent);
            }
        });

        mg3=(MagicButton)findViewById(R.id.magic_button3);
        mg3.setMagicButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mainhome.this , mybooks.class);
                startActivity(intent);
            }
        });
        fb1=(FloatingActionButton)findViewById(R.id.floatingbutton_add);
        fb1.setOnClickListener((new View.OnClickListener(){
            @Override
            public void onClick(View view){
                SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.clear();
                editor.commit();
                Intent out = new Intent(mainhome.this,login.class);
                startActivity(out);
                finish();
            }
        }));//shared pref */

        welcome=(TextView)findViewById(R.id.welcome);
        Bundle bundle=getIntent().getExtras();
        welcome.setText("Welcome, "+bundle.getString("name"));


        //code for displaying name and email in nav drawer
     /*   NavigationView navigationView = (NavigationView) findViewById(R.id.nav_drawer);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return false;
            }
        });
        View header=navigationView.getHeaderView(0);
/*View view=navigationView.inflateHeaderView(R.layout.nav_header_main);
        header_wel= (TextView)header.findViewById(R.id.header_name);
        header_email= (TextView)header.findViewById(R.id.header_email);
        header_wel.setText(bundle.getString("name"));
        header_email.setText(bundle.getString("email"));
        //code ends here*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
