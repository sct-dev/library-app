package com.sct_dev.lib_sctce;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class signup extends AppCompatActivity {

    Button button;
    EditText Name,User_name,Email,Pass,Con_pass,Branch,Year,Adm_num;
    String name,user_name,email,pass,con_pass,branch,year,adm_num;
    AlertDialog.Builder builder;
    String string_url="http://sctceonline.in/cs-dept-library/LIB-SCTCE/register.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        button=(Button)findViewById(R.id.reg_sign_up);
        Name=(EditText)findViewById(R.id.reg_name);
        User_name=(EditText)findViewById(R.id.reg_username);
        Email=(EditText)findViewById(R.id.reg_email);
        Pass=(EditText)findViewById(R.id.reg_pass);
        Con_pass=(EditText)findViewById(R.id.reg_con_pass);
        Branch=(EditText)findViewById(R.id.reg_branch);
        Year=(EditText)findViewById(R.id.reg_year);
        Adm_num=(EditText)findViewById(R.id.reg_Adm_num);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name=Name.getText().toString();
                user_name=User_name.getText().toString();
                email=Email.getText().toString();
                pass=Pass.getText().toString();
                con_pass=Con_pass.getText().toString();
                branch=Branch.getText().toString();
                year=Year.getText().toString();
                adm_num=Adm_num.getText().toString();


                if(name.equals("")||user_name.equals("")||email.equals("")||pass.equals("")||con_pass.equals("")||branch.equals("")||year.equals("")||adm_num.equals("")){
                    builder=new AlertDialog.Builder(signup.this);
                    builder.setTitle("Something went wrong...");
                    builder.setMessage("Please fill all the fields...");
                    displayAlert("input_error");
                }

                else{
                    if(!(pass.equals(con_pass))){
                        builder=new AlertDialog.Builder(signup.this);
                        builder.setTitle("Something went wrong...");
                        builder.setMessage("Your passwords are not matching...");
                        displayAlert("input_error");
                    }

                    else{
                        StringRequest sr = new StringRequest(Request.Method.POST, string_url,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        try {
                                            JSONArray jsonArray = new JSONArray(response);
                                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                                            String code =jsonObject.getString("code");
                                            String message =jsonObject.getString("message");
                                            builder=new AlertDialog.Builder(signup.this);
                                            builder.setTitle("Server response...");
                                            builder.setMessage(message);
                                            displayAlert(code);
                                        }catch (JSONException e){
                                            e.printStackTrace();
                                        }

                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getApplicationContext(),"Error connecting",Toast.LENGTH_SHORT).show();
                            }
                        }){
                            protected Map<String,String> getParams() throws AuthFailureError{
                                Map<String,String> params=new HashMap<String, String>();
                                params.put("name",name);
                                params.put("username",user_name);
                                params.put("password",pass);
                                params.put("email",email);
                                params.put("year",year);
                                params.put("admissionno",adm_num);
                                params.put("branch",branch);
                                return params;

                            }
                        };
                        MySingleton.getInstance(signup.this).addToRequestQue(sr);

                    }


                }
            }
        });
    }

    public void displayAlert(final String code){
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(code.equals("input_error")){
                    Pass.setText("");
                    Con_pass.setText("");
                }
                else if(code.equals("reg_success")){
                    finish();
                }
                else if(code.equals("reg_failed")){
                    Name.setText("");
                    Email.setText("");
                    User_name.setText("");
                    Adm_num.setText("");
                    Branch.setText("");
                    Pass.setText("");
                    Con_pass.setText("");
                    Year.setText("");
                }
            }
        });
        AlertDialog alertDialog=builder.create();
        alertDialog.show();
    }
}
