package com.sct_dev.lib_sctce;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Searchfra extends Fragment implements AdapterView.OnItemSelectedListener,View.OnClickListener{
    public static Searchfra newInstance() {
        Searchfra fragment = new Searchfra();
        return fragment;
    }


    private Spinner spinner;
    private static final String[] paths = {"Book name", "Author"};
    public String crit;
    public EditText searchcrit;
    private Button button;
    public String booklist[];
    public String authorlist[];
    public String booknauthor[];
    public ListView listView;
    int i;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View frview= inflater.inflate(R.layout.fragment_searchfra, container, false);
        searchcrit =(EditText) frview.findViewById(R.id.searchCrit);

        spinner = (Spinner) frview.findViewById(R.id.spinner2);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, paths);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        button=(Button) frview.findViewById(R.id.butbooksearch);
        listView=(ListView)frview.findViewById(R.id.list1);
        button.setOnClickListener(this);

        return  frview;

    }
    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {

        switch (position) {
            case 0:
                crit="bname";
                searchcrit.setHint("Book Name");
                break;
            case 1:
                crit="author";
                searchcrit.setHint("Author");
                break;
            default:crit="bname";


        }
    }
    public void onNothingSelected(AdapterView<?> parent){


    }
    @Override
    public void onClick(View v){

        searchbook(crit,searchcrit.getText().toString());
    }
    public void searchbook(String crite,String searchText){
        if(searchText.equals(""))
            Toast.makeText(getActivity(),"Enter Word for searching",Toast.LENGTH_SHORT).show();
        else{

            StringRequest stringRequest=new StringRequest(Request.Method.POST, "http://sctceonline.in/cs-dept-library/searchtest.php", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray result = jsonObject.getJSONArray("result");

                        booklist=new String[result.length()];
                        authorlist=new String[result.length()];
                        booknauthor=new String[result.length()];
                        for(i=0;i<result.length();i++){
                            JSONObject Data = result.getJSONObject(i);

                            booklist[i]=Data.getString("bname");
                            authorlist[i]=Data.getString("author");
                            booknauthor[i]=booklist[i]+" by "+authorlist[i];
                        }

                        Toast.makeText(getActivity(),"got result"+result.length(),Toast.LENGTH_SHORT).show();
                        ArrayAdapter<String> adapter=new ArrayAdapter<String>(getActivity(),R.layout.listlay,booknauthor);

                        ListView listView =(ListView)getView().findViewById(R.id.list1) ;
                        final BottomNavigationView mynv=getActivity().findViewById(R.id.navigation);
                        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
                            @Override
                            public void onScrollStateChanged(AbsListView absListView, int i) {
                                if(i==0){
                                    mynv.setVisibility(View.VISIBLE);
                                }
                                else{
                                    mynv.setVisibility(View.GONE);
                                }
                            }

                            @Override
                            public void onScroll(AbsListView absListView, int i, int i1, int i2) {

                            }


                        });
                        listView.setAdapter(adapter);
                        listView.setTextFilterEnabled(true);

                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            public void onItemClick(AdapterView<?> parent, View view,
                                                    int position, long id) {
                                Intent bukin=new Intent(getActivity(),bookinfo.class);
                                bukin.putExtra("bookname",booklist[position]);
                                bukin.putExtra("author",authorlist[position]);
                                // When clicked, show a toast with the TextView text
                                /*Toast.makeText(getApplicationContext(),
                                        ((TextView) view).getText(), Toast.LENGTH_SHORT).show();*/
                                startActivity(bukin);
                            }
                        });

                    } catch (JSONException e) {

                        e.printStackTrace();
                        Toast.makeText(getActivity(),"Cant search",Toast.LENGTH_SHORT).show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getActivity(),"Not able to Connect.. ,Please check your internet connection.",Toast.LENGTH_LONG).show();
                    error.printStackTrace();

                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> params=new HashMap<String, String>();
                    params.put("crit",crit);
                    params.put("value",searchcrit.getText().toString());
                    return params;
                }
            };
            MySingleton.getInstance(getActivity()).addToRequestQue(stringRequest);
        }
    }
}