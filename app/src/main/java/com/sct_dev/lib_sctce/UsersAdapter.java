package com.sct_dev.lib_sctce;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by muhammad on 12/11/17.
 */


class UsersAdapter extends ArrayAdapter<Booktra> {

    public UsersAdapter(Context context, ArrayList<Booktra> booktra) {

        super(context, 0, booktra);

    }



    @Override

    public View getView(int position, View convertView, ViewGroup parent) {

        // Get the data item for this position

        Booktra book = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view

        if (convertView == null) {

            convertView = LayoutInflater.from(getContext()).inflate(R.layout.customlist, parent, false);

        }

        // Lookup view for data population

        TextView bid = (TextView) convertView.findViewById(R.id.bookid);

        TextView cdt = (TextView) convertView.findViewById(R.id.chdate);

        TextView rdt = (TextView) convertView.findViewById(R.id.retdate);

        // Populate the data into the template view using the data object

        bid.setText(book.bookid);

        cdt.setText(book.chdate);

        rdt.setText(book.retdate);

        // Return the completed view to render on screen

        return convertView;

    }

}