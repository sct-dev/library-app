package com.sct_dev.lib_sctce;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class bookinfo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookinfo);
        Bundle binfo=getIntent().getExtras();
        TextView bn=(TextView)findViewById(R.id.bookname);
        TextView an=(TextView)findViewById(R.id.author);
        an.setText(binfo.getString("author"));
        bn.setText(binfo.getString("bookname"));
    }
}
